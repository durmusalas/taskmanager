import java.sql.*; // Import für SQL-Klassen
import java.util.Scanner; // Import für Scanner-Klasse, um Benutzereingaben zu lesen

public class TaskManagerApp {
    // Datenbankverbindungsinformationen
    private static final String url = "jdbc:mysql://localhost:3306/tasks"; // URL der Datenbank
    private static final String user = "root"; // Benutzername für die Datenbank
    private static final String password = "."; // Passwort für die Datenbank

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in); // Erstellen eines Scanner-Objekts für Benutzereingaben

        try (Connection conn = DriverManager.getConnection(url, user, password)) {
            // Verbindung zur Datenbank herstellen
            TaskManager taskManager = new TaskManager(conn); // Erstellen eines TaskManager-Objekts

            while (true) { // Unendliche Schleife für das Menü
                // Menüoptionen anzeigen
                System.out.println("\nTask Manager");
                System.out.println("1. Add Task");
                System.out.println("2. View Tasks");
                System.out.println("3. Update Task");
                System.out.println("4. Delete Task");
                System.out.println("5. Exit");
                System.out.print("Choose an option: ");

                int option = scanner.nextInt(); // Benutzereingabe lesen
                scanner.nextLine(); // Um den Zeilenumbruch nach dem Integer zu konsumieren

                // Auswahl der Benutzeroption
                switch (option) {
                    case 1:
                        taskManager.addTask(scanner); // Aufgabe hinzufügen
                        break;
                    case 2:
                        taskManager.viewTasks(); // Aufgaben anzeigen
                        break;
                    case 3:
                        taskManager.updateTask(scanner); // Aufgabe aktualisieren
                        break;
                    case 4:
                        taskManager.deleteTask(scanner); // Aufgabe löschen
                        break;
                    case 5:
                        System.out.println("Exiting..."); // Programm beenden
                        return;
                    default:
                        System.out.println("Invalid option. Please try again."); // Ungültige Option
                }
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Fehlerbehandlung für SQL-Exceptions
        }
    }
}
