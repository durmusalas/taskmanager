import java.sql.*;
import java.util.Scanner;

public class TaskManager {
    private Connection conn; // Verbindung zur Datenbank

    public TaskManager(Connection conn) {
        this.conn = conn; // Initialisieren der Datenbankverbindung
        initializeDB(); // Initialisieren der Datenbank
    }

    private void initializeDB() {
        // SQL-Befehl zum Erstellen der Tabelle, falls sie nicht existiert
        String createTableSQL = "CREATE TABLE IF NOT EXISTS tasks (" +
                                "id INT AUTO_INCREMENT PRIMARY KEY, " +
                                "title VARCHAR(100), " +
                                "description TEXT, " +
                                "due_date DATE)";
        try (Statement stmt = conn.createStatement()) {
            stmt.execute(createTableSQL); // Ausführen des SQL-Befehls
            System.out.println("Database initialized successfully.");
        } catch (SQLException e) {
            e.printStackTrace(); // Fehlerbehandlung
        }
    }

    public void addTask(Scanner scanner) {
        // Aufforderung zur Eingabe von Titel, Beschreibung und Fälligkeitsdatum der Aufgabe
        System.out.print("Enter task title: ");
        String title = scanner.nextLine();

        System.out.print("Enter task description: ");
        String description = scanner.nextLine();

        System.out.print("Enter task due date (YYYY-MM-DD): ");
        String dueDate = scanner.nextLine();

        // SQL-Befehl zum Einfügen einer neuen Aufgabe
        String sql = "INSERT INTO tasks (title, description, due_date) VALUES (?, ?, ?)";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, title); // Setzen des Titels
            pstmt.setString(2, description); // Setzen der Beschreibung
            pstmt.setDate(3, Date.valueOf(dueDate)); // Setzen des Fälligkeitsdatums
            pstmt.executeUpdate(); // Ausführen des Update-Befehls
            System.out.println("Task added successfully.");
        } catch (SQLException e) {
            e.printStackTrace(); // Fehlerbehandlung bei SQL-Fehlern
        }
    }

    public void viewTasks() {
        // SQL-Befehl zum Abrufen aller Aufgaben
        String sql = "SELECT * FROM tasks";
        try (Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery(sql)) {

            while (rs.next()) {
                // Abrufen von Daten aus dem ResultSet
                int id = rs.getInt("id");
                String title = rs.getString("title");
                String description = rs.getString("description");
                Date dueDate = rs.getDate("due_date"); // Abrufen des Fälligkeitsdatums
                // Ausgabe der Aufgaben
                System.out.println("ID: " + id + ", Title: " + title + ", Description: " + description + ", Due Date: " + dueDate);
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Fehlerbehandlung
        }
    }

    public void updateTask(Scanner scanner) {
        // Aufforderung zur Eingabe der ID der zu aktualisierenden Aufgabe
        System.out.print("Enter task ID to update: ");
        int id = scanner.nextInt();
        scanner.nextLine(); // Um den Zeilenumbruch nach der Zahleneingabe zu konsumieren

        // Aufforderung zur Eingabe neuer Titel und Beschreibung
        System.out.print("Enter new task title: ");
        String title = scanner.nextLine();

        System.out.print("Enter new task description: ");
        String description = scanner.nextLine();

        System.out.print("Enter new task due date (YYYY-MM-DD): ");
        String dueDate = scanner.nextLine();

        // SQL-Befehl zum Aktualisieren einer Aufgabe
        String sql = "UPDATE tasks SET title = ?, description = ?, due_date = ? WHERE id = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setString(1, title); // Setzen des neuen Titels
            pstmt.setString(2, description); // Setzen der neuen Beschreibung
            pstmt.setDate(3, Date.valueOf(dueDate)); // Setzen des neuen Fälligkeitsdatums
            pstmt.setInt(4, id); // Festlegen der ID der zu aktualisierenden Aufgabe
            int affectedRows = pstmt.executeUpdate(); // Ausführen des Update-Befehls

            if (affectedRows > 0) {
                System.out.println("Task updated successfully.");
            } else {
                System.out.println("Task not found."); // Wenn keine Aufgabe aktualisiert wurde
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Fehlerbehandlung
        }
    }

    public void deleteTask(Scanner scanner) {
        // Aufforderung zur Eingabe der ID der zu löschenden Aufgabe
        System.out.print("Enter task ID to delete: ");
        int id = scanner.nextInt();

        // SQL-Befehl zum Löschen einer Aufgabe
        String sql = "DELETE FROM tasks WHERE id = ?";
        try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
            pstmt.setInt(1, id); // Festlegen der ID der zu löschenden Aufgabe
            int affectedRows = pstmt.executeUpdate(); // Ausführen des Löschbefehls

            if (affectedRows > 0) {
                System.out.println("Task deleted successfully.");
            } else {
                System.out.println("Task not found."); // Wenn keine Aufgabe gelöscht wurde
            }
        } catch (SQLException e) {
            e.printStackTrace(); // Fehlerbehandlung
        }
    }
}







// import java.sql.*;
// import java.util.Scanner;

// public class TaskManager {
//     private Connection conn;

//     public TaskManager(Connection conn) {
//         this.conn = conn;
//         initializeDB();
//     }

//     private void initializeDB() {
//         String createTableSQL = "CREATE TABLE IF NOT EXISTS tasks (" +
//                                 "id INT AUTO_INCREMENT PRIMARY KEY, " +
//                                 "title VARCHAR(100), " +
//                                 "description TEXT, " +
//                                 "due_date DATE)";
//         try (Statement stmt = conn.createStatement()) {
//             stmt.execute(createTableSQL);
//             System.out.println("Database initialized successfully.");
//         } catch (SQLException e) {
//             e.printStackTrace();
//         }
//     }

//     public void addTask(Scanner scanner) {
//         System.out.print("Enter task title: ");
//         String title = scanner.nextLine();

//         System.out.print("Enter task description: ");
//         String description = scanner.nextLine();

//         String sql = "INSERT INTO tasks (title, description) VALUES (?, ?)";
//         try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
//             pstmt.setString(1, title);
//             pstmt.setString(2, description);
//             pstmt.executeUpdate();
//             System.out.println("Task added successfully.");
//         } catch (SQLException e) {
//             e.printStackTrace();
//         }
//     }

//     public void viewTasks() {
//         String sql = "SELECT * FROM tasks";
//         try (Statement stmt = conn.createStatement();
//              ResultSet rs = stmt.executeQuery(sql)) {

//             while (rs.next()) {
//                 int id = rs.getInt("id");
//                 String title = rs.getString("title");
//                 String description = rs.getString("description");
//                 System.out.println("ID: " + id + ", Title: " + title + ", Description: " + description);
//             }
//         } catch (SQLException e) {
//             e.printStackTrace();
//         }
//     }

//     public void updateTask(Scanner scanner) {
//         System.out.print("Enter task ID to update: ");
//         int id = scanner.nextInt();
//         scanner.nextLine(); // Um den Zeilenumbruch zu konsumieren

//         System.out.print("Enter new task title: ");
//         String title = scanner.nextLine();

//         System.out.print("Enter new task description: ");
//         String description = scanner.nextLine();

//         String sql = "UPDATE tasks SET title = ?, description = ? WHERE id = ?";
//         try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
//             pstmt.setString(1, title);
//             pstmt.setString(2, description);
//             pstmt.setInt(3, id);
//             int affectedRows = pstmt.executeUpdate();

//             if (affectedRows > 0) {
//                 System.out.println("Task updated successfully.");
//             } else {
//                 System.out.println("Task not found.");
//             }
//         } catch (SQLException e) {
//             e.printStackTrace();
//         }
//     }

//     public void deleteTask(Scanner scanner) {
//         System.out.print("Enter task ID to delete: ");
//         int id = scanner.nextInt();
//         scanner.nextLine(); // Um den Zeilenumbruch zu konsumieren

//         String sql = "DELETE FROM tasks WHERE id = ?";
//         try (PreparedStatement pstmt = conn.prepareStatement(sql)) {
//             pstmt.setInt(1, id);
//             int affectedRows = pstmt.executeUpdate();

//             if (affectedRows > 0) {
//                 System.out.println("Task deleted successfully.");
//             } else {
//                 System.out.println("Task not found.");
//             }
//         } catch (SQLException e) {
//             e.printStackTrace();
//         }
//     }
// }
